<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'phpmailer/Exception.php';
require 'phpmailer/PHPMailer.php';
require 'phpmailer/SMTP.php';


function send_mail($site_url, $to, $to_name, $subject, $cart, $user, $cc = null)
{
    $products = "";
    $users = "";
    foreach ($cart as $i) {
        $image = explode(',', $i['images']);
        $final_price = $i['quantity'] * $i['price'];
        $id = $i['id'];
        $products = $products . '<tr>
            <td>
                <a href="'.$site_url.'uploads/product_' . $id . '/' . $image[0] . '">View Image</a>
            </td>
            <td>
                <a class="aa-cart-title" href="' . $site_url . 'product.php?product=' . $i['id'] . '">' . $i['title'] . '</a>
            </td>
            <td>LKR ' . $i['price'] . '</td>
            <td>' . $i['quantity'] . '</td>
            <td><b>LKR ' . $final_price . '</b></td>
        </tr>';
    }
    $name = $user['name'];
    $phone = $user['phone'];
    $email = $user['email'];
    $postalcode = $user['postalcode'];
    $city = $user['city'];
    $address = $user['address'];

    $users =$users ."<p>Name : ".$name."</p>";
    $users =$users ."<p>Phone : ".$phone."</p>";
    $users =$users ."<p>Email : ".$email."</p>";
    $users =$users ."<p>Postal Code : ".$postalcode."</p>";
    $users =$users ."<p>City/Town : ".$city."</p>";
    $users =$users ."<p>Address : ".$address."</p>";


    $body = "<div style='margin-left:auto; margin-right: auto;padding:50px;width:600px;height:572px;font-family: Arial, Helvetica, sans-serif;'>
    <h1 style='color:#222;font-family: Arial, Helvetica, sans-serif;text-align:center;line-height:1.5em;text-transform: uppercase'>
        Order Success!
    </h1>
    <hr>
    <br>
    <table width=\"100%\" border=\"1\" cellspacing=\"0\" style=\"text-align: center; border-color: #ccc;\">
        <thead>
        <tr>
            <th></th>
            <th>Product</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Total</th>
        </tr>
        </thead>
        <tbody>" . $products . "</tbody>
    </table>
    <br>

    <hr>
    <h2 style=\"text-align: center;text-transform: uppercase\">Billing Infomation</h2>
    <hr>".$users." <br>
    <hr>
    <p style=\"text-align: center;text-transform: uppercase;font-size: 13px;\">slim and curly <br> all rights reserved</p>
</div>";
    $from = 'sales@slimandcurly.tk';

    $host = 'smtp.gmail.com';
    $username = "slimandcurlyweb@gmail.com";
    $password = "slim123*";
    $port = 587;

    $mail = new PHPMailer;
    $mail->isSMTP();
    $mail->SMTPDebug = 0;
    $mail->Host = "smtp.gmail.com";
    $mail->Port = 587; // TLS only
    $mail->SMTPSecure = 'tls';
    $mail->SMTPAuth = true;
    $mail->Username = $username;
    $mail->Password = $password;
//    $mail->isHTML(true);
//    $mail->addCustomHeader('Content-Type', 'image/jpeg');
    $mail->setFrom($from, 'Slim & Curry');
    $mail->addAddress($to, $to_name);
    $mail->addCC($cc);
    $mail->Subject = $subject;
    $mail->msgHTML($body);
    $mail->AltBody = 'HTML messaging not supported';
// $mail->addAttachment('images/phpmailer_mini.png'); //Attach an image file

    if (!$mail->send()) {
        return false;
    } else {
        return true;

    }

}

?>
<?php

$success = "";
$reg_success = '';
$order_success = '';
$success_order = '';

function get_single_product_details($con, $id)
{
    $query = "SELECT * FROM products where id=" . $id . " limit 1";
    $res = mysqli_query($con, $query);
    $result = mysqli_fetch_array($res, MYSQLI_ASSOC);

    return $result;
}

function get_product_category($con, $id)
{
    $query = "SELECT category_name FROM categories where id=" . $id . " limit 1";
    $res = mysqli_query($con, $query);
    $result = mysqli_fetch_array($res, MYSQLI_ASSOC);

    return $result;
}

function logout()
{
    unset($_SESSION["user_id"]);
    unset($_SESSION["email"]);
    unset($_SESSION["name"]);
    header("Location: home.php");
}

function save_order($site_url,$post, $cart, $con)
{
    $user_id = $_SESSION["user_id"];
    $name = $post['name'];
    $phone = $post['phone'];
    $email = $post['email'];
    $postalcode = $post['postalcode'];
    $city = $post['city'];
    $address = $post['address'];
    $total_quantity = $post['total_quantity'];
    $total_price = $post['total_price'];
    $cart_data = json_encode($cart);
    $ext = mysqli_num_rows(mysqli_query($con, "SELECT * FROM user_details WHERE user_id='$user_id' AND status=1"));
//    var_dump($ext);
    $query = mysqli_query($con, "INSERT INTO user_details (user_id, name, email, phone, address, city, postalcode)VALUES('$user_id','$name','$email', '$phone', '$address', '$city','$postalcode')");
//    if (!$ext) {
//
//    }
//    $user_det = mysqli_fetch_array(mysqli_query($con, "SELECT * FROM user_details WHERE user_id='$user_id' AND status=1"), MYSQLI_ASSOC);
    $user_details_id = mysqli_insert_id($con);
    $query = mysqli_query($con, "INSERT INTO orders (user_id, user_details_id, qty, price, product_details) VALUES('$user_id','$user_details_id', '$total_quantity', '$total_price', '$cart_data')");

    foreach ($cart as $item) {
        $product_id = $item['id'];

        $product_det = mysqli_fetch_array(mysqli_query($con, "SELECT * FROM products WHERE id='$product_id' AND status=1"), MYSQLI_ASSOC);
        $ext_qty = $product_det['available_qty'];
        $qty = $item['quantity'];
        if ($ext_qty == 0) {
            $final_qty = 0;
        } else {
            $final_qty = $ext_qty - $qty;
        }
        $query = mysqli_query($con, "UPDATE products SET available_qty=$final_qty WHERE id = $product_id");
    }
    $to = $email;
    $cc = 'slimandcurlyweb@gmail.com';
    $subject = 'Item Order on '.date('Y-m-d H:i:s');
//    var_dump($post['name']);
    send_mail($site_url,$email,$name,$subject,$cart,$post,$cc);
    unset($_SESSION["cart_item"]);

    $order_success = 'order_success();';
    header('location:my-account.php?order_success=1');

}

function get_user($user_id, $con)
{
    $user_det = mysqli_fetch_array(mysqli_query($con, "SELECT * FROM user WHERE id='$user_id'"), MYSQLI_ASSOC);
    return $user_det;
}

function get_user_details($user_id, $con)
{
    $user_det = mysqli_fetch_array(mysqli_query($con, "SELECT * FROM user_details WHERE user_id='$user_id'"), MYSQLI_ASSOC);
    return $user_det;
}

function get_user_orders($user_id, $con)
{
    $user_det = mysqli_fetch_all(mysqli_query($con, "SELECT * FROM orders"), MYSQLI_ASSOC);
    return $user_det;
}

function get_single_user_details($user_details_id, $con)
{
    $user_det = mysqli_fetch_array(mysqli_query($con, "SELECT * FROM user_details WHERE id='$user_details_id'"), MYSQLI_ASSOC);
    return $user_det;
}

function get_all_products($con)
{
    $sql = mysqli_query($con, "SELECT * FROM products WHERE status = 1");
    $products = mysqli_fetch_all($sql, MYSQLI_ASSOC);
    return $products;
}

function get_latest_products($con)
{
    $sql = mysqli_query($con, "SELECT * FROM products WHERE status = 1 ORDER BY created_at DESC limit 8");
    $products = mysqli_fetch_all($sql, MYSQLI_ASSOC);
    return $products;
}

function get_categories($con)
{
    $sql = mysqli_query($con, "SELECT * FROM categories WHERE status = 1");
    $cats = mysqli_fetch_all($sql, MYSQLI_ASSOC);
    return $cats;
}

function get_sub_categories($con,$cat=null)
{
    $category='';
    if ($cat != null){
        $category = "category ='$cat' AND";
    }
    $sql = mysqli_query($con, "SELECT * FROM sub_categories WHERE $category status = 1");
    $cats = mysqli_fetch_all($sql, MYSQLI_ASSOC);
    return $cats;
}

function get_sub_sub_categories($con,$cat=null,$sub_cat=null)
{
    $sub_category ='';
    $category='';
    if ($cat != null){
        $category = "category ='$cat' AND";
    }
    if ($sub_cat != null){
        $sub_category = "sub_category='$sub_cat' AND";
    }
    $sql = mysqli_query($con, "SELECT * FROM sub_sub_categories WHERE $category $sub_category status = 1");
    $cats = mysqli_fetch_all($sql, MYSQLI_ASSOC);
    return $cats;
}

function filter_products($con, $category = null, $sub_category = null, $sub_sub_category= null){
    $cat='';
    $sub_cat ='';
    $sub_sub_cat ='';
    if ($category != null){
        $cat = "category ='$category' AND";
    }
    if ($sub_category != null){
        $sub_cat = "sub_category='$sub_category' AND";
    }
    if ($sub_sub_category != null){
        $sub_sub_cat = "sub_sub_category='$sub_sub_category' AND";
    }
    $sql = mysqli_query($con, "SELECT * FROM products WHERE $cat $sub_cat $sub_sub_cat status = 1");
    $products = mysqli_fetch_all($sql, MYSQLI_ASSOC);
    return $products;
}
function get_category_name($con, $id, $type){
    if($type == 'category'){
        $sql = mysqli_query($con, "SELECT * FROM categories WHERE id='$id' AND status = 1");
        $cats = mysqli_fetch_array($sql, MYSQLI_ASSOC);
        return $cats['category_name'];
    }else if($type == 'sub_category'){
        $sql = mysqli_query($con, "SELECT * FROM sub_categories WHERE id='$id' AND status = 1");
        $cats = mysqli_fetch_array($sql, MYSQLI_ASSOC);
        return $cats['sub_category_name'];
    }else if($type == 'sub_sub_category'){
        $sql = mysqli_query($con, "SELECT * FROM sub_sub_categories WHERE id='$id' AND status = 1");
        $cats = mysqli_fetch_array($sql, MYSQLI_ASSOC);
        return $cats['sub_sub_category_name'];
    }

}

?>
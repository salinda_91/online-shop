<?php
//session_start();

// initializing variables
$username = "";
$email    = "";
$errors = array(); 

// connect to the database
//$con = mysqli_connect('localhost', 'root', '', 'shopping');

// REGISTER USER
if (isset($_POST['reg_user'])) {
  // receive all input values from the form
  $username = mysqli_real_escape_string($con, $_POST['username']);
  $email = mysqli_real_escape_string($con, $_POST['email']);
  $password_1 = mysqli_real_escape_string($con, $_POST['password_1']);
  $password_2 = mysqli_real_escape_string($con, $_POST['password_2']);

  // form validation: ensure that the form is correctly filled ...
  // by adding (array_push()) corresponding error unto $errors array
  if (empty($username)) { array_push($errors, "Username is required"); }
  if (empty($email)) { array_push($errors, "Email is required"); }
  if (empty($password_1)) { array_push($errors, "Password is required"); }
  if ($password_1 != $password_2) {
  array_push($errors, "The two passwords do not match");
  }

  // first check the database to make sure 
  // a user does not already exist with the same username and/or email
  $user_check_query = "SELECT * FROM user WHERE email='$email' LIMIT 1";
  $result = mysqli_query($con, $user_check_query);
  $user = mysqli_fetch_assoc($result);
  
  if ($user) { // if user exists
    if ($user['email'] === $email) {
      array_push($errors, "email already exists");
    }
  }
$status = "1";
  // Finally, register user if there are no errors in the form
  if (count($errors) == 0) {
    $password = md5($password_1);//encrypt the password before saving in the database

    $query = "INSERT INTO user (name, email, password, status) 
          VALUES('$username', '$email', '$password','$status')";
    mysqli_query($con, $query);
    $_SESSION['username'] = $username;
    $_SESSION['success'] = "You are now logged in";
        $reg_success = "register_success();";
  }
}

// LOGIN USER
if (isset($_POST['login_user'])) {
  $email = mysqli_real_escape_string($con, $_POST['email']);
//  var_dump($email);
  $password = mysqli_real_escape_string($con, $_POST['password']);
//  var_dump($password);
  $password2=md5($password);
//  var_dump($password2);



  if (empty($email)) {
    array_push($errors, "email is required");
  }
  if (empty($password)) {
    array_push($errors, "Password is required");
  }

  if (count($errors) == 0) {
    $password = md5($password);
//    $query = "SELECT * FROM user where ";
      $query="SELECT * FROM user WHERE email='$email' AND password='$password2' AND status='1' limit 1";
    $results = mysqli_query($con, $query);
    if (mysqli_num_rows($results) == 1) {
        $user_details = mysqli_fetch_array($results,MYSQLI_ASSOC);
//        var_dump($user_details);
      $_SESSION['user_id'] = $user_details['id'];
      $_SESSION['name'] = $user_details['name'];
      $_SESSION['email'] = $email;

      if (isset($_GET['return'])){
          $retun = $_GET['return'];
          header("location:".$retun);
      }else{
          header('location:home.php');
      }

    }else {
      array_push($errors, "Wrong username/password combination");
    }
  }
}
?>
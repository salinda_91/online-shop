<?php
session_start();
include_once "config/config.php";
include_once "config/function.php";

if (isset($_GET['product'])){
    $user = 1;
    $id = $_GET['product'];
    $product = get_single_product_details($con,$id);
    $availability = ($product['available_qty']>0)? true : false;
    $category = get_product_category($con,$product['category']);
    $category_name = $category['category_name'];

}else{
    header('location:index.php');
}
require "add_to_cart.php";
include 'views/product-detail.php';
?>

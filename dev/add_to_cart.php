<?php
if (isset($_GET['product'])){
    $id = $_GET['product'];
}
if (isset($_GET["action"])) {
    switch ($_GET["action"]) {
        case "add":
            if (!empty($_POST["quantity"])) {
                $productByCode = get_single_product_details($con, $id);
//                exit();
                $itemArray = array($id => array('id' => $id ,'title' => rtrim($productByCode["title"]), 'images' => $productByCode["images"], 'quantity' => $_POST["quantity"], 'price' => $productByCode["unit_price"]));
//                var_dump($itemArray);

                if (!empty($_SESSION["cart_item"])) {
                    if (in_array($id, array_keys($_SESSION["cart_item"]))) {
                        foreach ($_SESSION["cart_item"] as $k => $v) {

                            if ($id == $k) {
                                if (empty($_SESSION["cart_item"][$k]["quantity"])) {
                                    $_SESSION["cart_item"][$k]["quantity"] = 0;
                                }
                                $_SESSION["cart_item"][$k]["quantity"] += $_POST["quantity"];
                            }
                        }
                        $success = 'alert_msg();';
                    } else {
                        $_SESSION["cart_item"] = array_merge($_SESSION["cart_item"], $itemArray);
                        $success = 'alert_msg();';
                    }
                } else {
                    $_SESSION["cart_item"] = $itemArray;
                    $success = 'alert_msg();';
                }

            }
            break;
        case "remove":
            if (!empty($_SESSION["cart_item"])) {
                foreach ($_SESSION["cart_item"] as $k => $v) {
                    if ($_GET["product"] == $k)
                        unset($_SESSION["cart_item"][$k]);
                    if (empty($_SESSION["cart_item"]))
                        unset($_SESSION["cart_item"]);
                }
            }
            break;
        case "empty":
            unset($_SESSION["cart_item"]);
            break;
    }
}


?>
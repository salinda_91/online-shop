<?php
session_start();
include_once "config/config.php";
include_once "config/function.php";
$is_cart = true;
if (!isset($_SESSION["cart_item"])) {
    if (empty($_SESSION["cart_item"])) {
        header('location:shopping-cart.php');
    }
    header('location:shopping-cart.php');
}

if (isset($_POST['total_price']) && isset($_POST['total_quantity'])){
    save_order($site_url, $_POST,$_SESSION['cart_item'], $con);
}
$name= '';
$email = '';
$phone = '';
$address ='';
$city='';
$postalcode='';
$is_loged = false;

if (isset($_SESSION['user_id']) && isset($_SESSION['email'])) {
    $is_loged =true;
    $name= $_SESSION['name'];
    $email = $_SESSION['email'];
}
//var_dump($_SESSION['cart_item']);
require "add_to_cart.php";
include 'views/checkout.php';
?>

 <?php include 'blocks/header.php';?>

  <!-- catg header banner section -->
  <section id="aa-catg-head-banner">
      <img src="<?php echo $site_url; ?>views/assets/img/fashion/fashion-header-bg-8.jpg" alt="fashion img">
      <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <h2>Login</h2>
        <ol class="breadcrumb">
          <li><a href="<?php echo $site_url;?>">Home</a></li>
          <li class="active">Login</li>
        </ol>
      </div>
     </div>
   </div>
  </section>
  <!-- / catg header banner section -->

 <!-- Cart view section -->
 <section id="aa-myaccount">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
        <div class="aa-myaccount-area">         
            <div class="row">
              <div class="col-md-6 col-md-offset-3">
                <div class="aa-myaccount-login">
                <h4>Login</h4>
                    <p class="er"><?php include('config/errors.php'); ?></p>

                 <form  class="aa-login-form"  method="post" action="<?php echo $_SERVER['REQUEST_URI'] ?>">
                  <label for=""> Email address<span>*</span></label>
                   <input type="text" placeholder="Email address" name="email">
                   <label for="">Password<span>*</span></label>
                    <input type="password" placeholder="Password" name="password">
                    <button type="submit" class="aa-browse-btn" name="login_user">Login</button>
<!--                    <label class="rememberme" for="rememberme"><input type="checkbox" id="rememberme"> Remember me </label>-->
<!--                    <p class="aa-lost-password"><a href="#">Lost your password?</a></p>-->
                    <p class="aa-lost-password text-right"><a href="<?php echo $site_url; ?>register.php">User Register</a></p>
                  </form>
                </div>
              </div>
         
            </div>          
         </div>
       </div>
     </div>
   </div>
 </section>
 <!-- / Cart view section -->

<?php include 'blocks/footer.php';?>
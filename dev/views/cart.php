<?php include 'blocks/header.php'; ?>

<!-- catg header banner section -->
<section id="aa-catg-head-banner">
    <img src="<?php echo $site_url; ?>views/assets/img/fashion/fashion-header-bg-8.jpg" alt="fashion img">
    <div class="aa-catg-head-banner-area">
        <div class="container">
            <div class="aa-catg-head-banner-content">
                <h2>Shopping Cart</h2>
                <ol class="breadcrumb">
                    <li><a href="<?php echo $site_url; ?>">Home</a></li>
                    <li class="active">Shopping Cart</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- / catg header banner section -->

<!-- Cart view section -->
<section id="cart-view">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="cart-view-area">
                    <div class="cart-view-table">
                        <form action="">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th>Product</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $total_quantity = 0;
                                    $total_price = 0;
                                    if (isset($_SESSION["cart_item"])) {

                                        foreach ($_SESSION["cart_item"] as $item) {
                                            $item_price = $item["quantity"] * $item["price"];
                                            $image = explode(',', $item['images']);
                                            $uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
                                            $url = 'http://' . $_SERVER['HTTP_HOST'] . $uri_parts[0];
                                            ?>
                                            <tr>
                                                <td><a class="remove" href="<?php echo $url; ?>?product=<?php echo $item["id"]; ?>&action=remove">
                                                        <fa class="fa fa-close"></fa>
                                                    </a></td>
                                                <td><a href="#"><img src="<?php echo $site_url; ?>uploads/product_<?php echo $item['id']; ?>/<?php echo $image[0]; ?>" alt="img"></a></td>
                                                <td><a class="aa-cart-title" href="<?php echo $site_url; ?>product.php?product=<?php echo $item['id']; ?>"><?php echo $item['title']; ?></a></td>
                                                <td>LKR <?php echo $item["price"]; ?></td>
                                                <td><?php echo $item["quantity"]; ?></td>
                                                <td><b>LKR <?php echo $item_price ?></b></td>
                                            </tr>

                                            <?php
                                            $total_quantity += $item["quantity"];
                                            $total_price += ($item["price"] * $item["quantity"]);
                                        } ?>
                                        <tr>
                                            <td colspan="6" >
                                                <a class="aa-cartbox-checkout aa-primary-btn empty-cart" href="<?php echo $url; ?>?action=empty">Empty Cart</a>
                                            </td>
                                        </tr>
                                        <?php
                                    }else{
                                        ?>
                                        <tr>
                                            <td colspan="6" class="aa-cart-view-bottom">
                                                <span class="aa-cartbox-total-title"><b>No items selected</b></span>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    <tr>
                                        <td colspan="6" class="aa-cart-view-bottom">
                                            <a class="aa-cart-view-btn" href="<?php echo $site_url; ?>product-list.php">Continue Shopping</a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </form>
                        <!-- Cart Total view -->
                        <div class="cart-view-total">
                            <h4>Cart Totals</h4>
                            <table class="aa-totals-table">
                                <tbody>
                                <tr>
                                    <th>Total Items</th>
                                    <td><?php echo $total_quantity; ?></td>
                                </tr>
                                <tr>
                                    <th>Total</th>
                                    <td><?php echo $total_price; ?></td>
                                </tr>
<!--                                <tr>-->
<!--                                    <th>Total</th>-->
<!--                                    <td>LKR 4850.00</td>-->
<!--                                </tr>-->
                                </tbody>
                            </table>
                            <a href="<?php echo $site_url; ?>checkout.php" class="aa-cart-view-btn">Proceed to Checkout</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- / Cart view section -->


<?php include 'blocks/footer.php'; ?>
<?php include 'blocks/header.php'; ?>
    <!-- catg header banner section -->
    <section id="aa-catg-head-banner">
        <img src="<?php echo $site_url; ?>views/assets/img/fashion/fashion-header-bg-8.jpg" alt="fashion img">
        <div class="aa-catg-head-banner-area">
            <div class="container">
                <div class="aa-catg-head-banner-content">
                    <h2><?php echo $product['title']; ?></h2>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo $site_url; ?>">Home</a></li>
                        <li><a href="#">Product</a></li>
                        <li class="active"><?php echo $product['title']; ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!-- / catg header banner section -->

    <!-- product category -->
    <section id="aa-product-details">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="aa-product-details-area">
                        <div class="aa-product-details-content">
                            <div class="row">
                                <!-- Modal view slider -->
                                <div class="col-md-5 col-sm-5 col-xs-12">
                                    <div class="aa-product-view-slider">
                                        <div id="demo-1" class="simpleLens-gallery-container">
                                            <div class="simpleLens-container">
                                                <?php
                                                $images = explode(",", $product['images']);
                                                ?>
                                                <div class="simpleLens-big-image-container"><a
                                                            data-lens-image="<?php echo $site_url; ?>uploads/product_<?php echo $id; ?>/large/<?php echo $images[0]; ?>"
                                                            class="simpleLens-lens-image">
                                                        <img src="<?php echo $site_url; ?>uploads/product_<?php echo $id; ?>/<?php echo $images[0]; ?>"
                                                             class="simpleLens-big-image"></a>
                                                </div>
                                            </div>
                                            <div class="simpleLens-thumbnails-container">
                                                <?php

                                                foreach ($images as $img) {
                                                    ?>
                                                    <a data-big-image="<?php echo $site_url; ?>uploads/product_<?php echo $id; ?>/<?php echo $img; ?>"
                                                       data-lens-image="<?php echo $site_url; ?>uploads/product_<?php echo $id; ?>/large/<?php echo $img; ?>"
                                                       class="simpleLens-thumbnail-wrapper" href="#">
                                                        <img src="<?php echo $site_url; ?>uploads/product_<?php echo $id; ?>/thumb/<?php echo $img; ?>">
                                                    </a>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Modal view content -->
                                <div class="col-md-7 col-sm-7 col-xs-12">
                                    <div class="aa-product-view-content">
                                        <h3><?php echo $product['title']; ?></h3>
                                        <div class="aa-price-block">
                                            <span class="aa-product-view-price">LKR <?php echo $product['unit_price']; ?></span>
                                            <p class="aa-product-avilability">
                                                Avilability: <?php echo ($availability) ? '<span>In stock</span>' : '<span>Out of stock</span>'; ?></p>
                                        </div>
                                        <p><?php echo $product['short_description']; ?></p>
                                        <h4>Size</h4>
                                        <div class="aa-prod-view-size">
                                            <?php
                                            $sizes = explode(",", $product['available_sizes']);
                                            foreach ($sizes as $size) {
                                                ?>
                                                <a href="#"><?php echo strtoupper($size); ?></a>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                        <h4>Color</h4>
                                        <div class="aa-color-tag">
                                            <?php
                                            $colors = explode(",", $product['available_colors']);
                                            foreach ($colors as $color) {
                                                ?>
                                                <a href="#" style="background: <?php echo $color ?>"></a>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                        <h4>Quantity</h4>
                                        <form method="post"
                                              action="<?php echo $site_url; ?>product.php?product=<?php echo $id; ?>&&action=add">

                                            <div class="aa-prod-quantity">
                                                <input type="number" min="1" value="1" name="quantity" id="qty">
                                                <p class="aa-prod-category">
                                                    Category: <a href="#"><?php echo $category_name; ?></a>
                                                </p>
                                            </div>
                                            <div class="aa-prod-view-bottom">
                                                <button class="aa-add-to-cart-btn" type="submit" <?php echo (!$availability) ? 'disabled' : ''; ?>>
                                                    Add To Cart
                                                </button>

                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="aa-product-details-bottom">
                            <ul class="nav nav-tabs" id="myTab2">
                                <li><a href="#description" data-toggle="tab">Description</a></li>
                                <!--                                <li><a href="#review" data-toggle="tab">Reviews</a></li>-->
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="description">
                                    <?php echo $product['long_description']; ?>
                                </div>
                                <!--                                <div class="tab-pane fade " id="review">-->
                                <!--                                    <div class="aa-product-review-area">-->
                                <!--                                        <h4>2 Reviews for T-Shirt</h4>-->
                                <!--                                        <ul class="aa-review-nav">-->
                                <!--                                            <li>-->
                                <!--                                                <div class="media">-->
                                <!--                                                    <div class="media-left">-->
                                <!--                                                        <a href="#">-->
                                <!--                                                            <img class="media-object" src="img/testimonial-img-3.jpg"-->
                                <!--                                                                 alt="girl image">-->
                                <!--                                                        </a>-->
                                <!--                                                    </div>-->
                                <!--                                                    <div class="media-body">-->
                                <!--                                                        <h4 class="media-heading"><strong>Marla Jobs</strong> - <span>March 26, 2016</span>-->
                                <!--                                                        </h4>-->
                                <!--                                                        <div class="aa-product-rating">-->
                                <!--                                                            <span class="fa fa-star"></span>-->
                                <!--                                                            <span class="fa fa-star"></span>-->
                                <!--                                                            <span class="fa fa-star"></span>-->
                                <!--                                                            <span class="fa fa-star"></span>-->
                                <!--                                                            <span class="fa fa-star-o"></span>-->
                                <!--                                                        </div>-->
                                <!--                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>-->
                                <!--                                                    </div>-->
                                <!--                                                </div>-->
                                <!--                                            </li>-->
                                <!--                                            <li>-->
                                <!--                                                <div class="media">-->
                                <!--                                                    <div class="media-left">-->
                                <!--                                                        <a href="#">-->
                                <!--                                                            <img class="media-object" src="img/testimonial-img-3.jpg"-->
                                <!--                                                                 alt="girl image">-->
                                <!--                                                        </a>-->
                                <!--                                                    </div>-->
                                <!--                                                    <div class="media-body">-->
                                <!--                                                        <h4 class="media-heading"><strong>Marla Jobs</strong> - <span>March 26, 2016</span>-->
                                <!--                                                        </h4>-->
                                <!--                                                        <div class="aa-product-rating">-->
                                <!--                                                            <span class="fa fa-star"></span>-->
                                <!--                                                            <span class="fa fa-star"></span>-->
                                <!--                                                            <span class="fa fa-star"></span>-->
                                <!--                                                            <span class="fa fa-star"></span>-->
                                <!--                                                            <span class="fa fa-star-o"></span>-->
                                <!--                                                        </div>-->
                                <!--                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>-->
                                <!--                                                    </div>-->
                                <!--                                                </div>-->
                                <!--                                            </li>-->
                                <!--                                        </ul>-->
                                <!--                                        <h4>Add a review</h4>-->
                                <!--                                        <div class="aa-your-rating">-->
                                <!--                                            <p>Your Rating</p>-->
                                <!--                                            <a href="#"><span class="fa fa-star-o"></span></a>-->
                                <!--                                            <a href="#"><span class="fa fa-star-o"></span></a>-->
                                <!--                                            <a href="#"><span class="fa fa-star-o"></span></a>-->
                                <!--                                            <a href="#"><span class="fa fa-star-o"></span></a>-->
                                <!--                                            <a href="#"><span class="fa fa-star-o"></span></a>-->
                                <!--                                        </div>-->
                                <!--                                        <!-- review form -->-->
                                <!--                                        <form action="" class="aa-review-form">-->
                                <!--                                            <div class="form-group">-->
                                <!--                                                <label for="message">Your Review</label>-->
                                <!--                                                <textarea class="form-control" rows="3" id="message"></textarea>-->
                                <!--                                            </div>-->
                                <!--                                            <div class="form-group">-->
                                <!--                                                <label for="name">Name</label>-->
                                <!--                                                <input type="text" class="form-control" id="name" placeholder="Name">-->
                                <!--                                            </div>-->
                                <!--                                            <div class="form-group">-->
                                <!--                                                <label for="email">Email</label>-->
                                <!--                                                <input type="email" class="form-control" id="email"-->
                                <!--                                                       placeholder="example@gmail.com">-->
                                <!--                                            </div>-->
                                <!---->
                                <!--                                            <button type="submit" class="btn btn-default aa-review-submit">Submit-->
                                <!--                                            </button>-->
                                <!--                                        </form>-->
                                <!--                                    </div>-->
                                <!--                                </div>-->
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- / product category -->

<?php include 'blocks/footer.php'; ?>
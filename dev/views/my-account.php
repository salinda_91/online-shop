<?php include 'blocks/header.php'; ?>
    <!-- catg header banner section -->
    <section id="aa-catg-head-banner">
        <img src="<?php echo $site_url; ?>views/assets/img/fashion/fashion-header-bg-8.jpg" alt="fashion img">
        <div class="aa-catg-head-banner-area">
            <div class="container">
                <div class="aa-catg-head-banner-content">
                    <h2>My Account</h2>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo $site_url; ?>">Home</a></li>
                        <li class="active">My Account</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!-- / catg header banner section -->

    <!-- Cart view section -->
    <section id="aa-myaccount">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="aa-myaccount-area">
                        <div class="row">

                            <div class="col-md-3">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                                <figure>
                                     <img src="http://www.localcrimenews.com/wp-content/uploads/2013/07/default-user-icon-profile.png" alt="" class="img-circle" style="width:75px;" id="user-img">
                                </figure>
                                <h5 style="text-align:center;"><strong id="user-name"><?php echo $user['name'] ?></strong></h5>
                                
                                <p style="text-align:center;font-size: smaller;overflow-wrap: break-word;" id="user-email"><?php echo $user['email'] ?></p>
                                <p style="text-align:center;font-size: smaller;"><strong>Phone : <?php echo $user_details['phone'] ?> </strong><span class="tags" id="user-status"><?php echo $user_details['address'] ?></span></p>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 divider text-center"></div>
                                <p style="text-align:center;font-size: smaller;"><strong><?php echo $user_details['address'] ?></strong></p>
                                <p style="text-align:center;font-size: smaller;" id="user-role">City : <?php echo $user_details['city'] ?></p>
                                   <p style="text-align:center;font-size: smaller;" id="user-role">Postal Code : <?php echo $user_details['postalcode'] ?></p>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 divider text-center"></div>
                                   
                              </div>
                           

                            </div>
                            <div class="col-md-9">
    

                                <h3>Purchase History</h3>
                                <div class="aa-myaccount-register">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $x = 1;
                                            foreach ($order_details as $item) {
                                                $products = json_decode(trim($item['product_details']));
//                                                var_dump($products);
//                                                var_dump($products);
                                                ?>

                                                <tr>
                                                    <td><?php echo $x; ?></td>
                                                    <td><?php echo date_format(date_create($item['ordered_at']),"Y-M-d h:i a"); ?></td>
                                                    <td colspan="2" class="text-right">Items -
                                                        <b><?php echo $item['qty'] ?></b></td>
                                                    <td colspan="3" class="text-right">Total -
                                                        <b>LKR <?php echo $item['price'] ?></b></td>
                                                </tr>
                                                <?php
//                                                var_dump($item['product_details']);
//                                                var_dump($products);
                                                if (!empty($products)) {
                                                    foreach ($products as $pd) {
                                                        $image = explode(',', $pd->images);
                                                        ?>
                                                        <tr style="background: #eee">
                                                            <td></td>
                                                            <td><?php echo $pd->title; ?></td>
                                                            <td>
                                                                <img src="<?php echo $site_url; ?>uploads/product_<?php echo $pd->id; ?>/<?php echo $image[0]; ?>"
                                                                     alt="img" width="80px">
                                                            </td>
                                                            <td><?php echo $pd->quantity; ?> X
                                                                LKR <?php echo $pd->price; ?></td>
                                                            <td class="text-right">
                                                                LKR <?php echo $pd->quantity * $pd->price; ?></td>

                                                        </tr>
                                                <?php
                                                    }
                                                }
                                                ?>

                                                <tr>
                                                    <?php $u_info = get_single_user_details($item['user_details_id'], $con); ?>
                                                    <td></td>
                                                    <td colspan="3">Address : <b><?php echo $u_info['address'] ?></b>
                                                        <br>
                                                        City : <b><?php echo $u_info['city'] ?></b></td>
                                                    <td colspan="4">Phone : <b><?php echo $u_info['phone'] ?></b><br>
                                                        Postal Code : <b><?php echo $u_info['postalcode'] ?></b></td>
                                                </tr>
                                           

                                                <?php $x++;
                                            } ?>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- / Cart view section -->


<?php include 'blocks/footer.php'; ?>
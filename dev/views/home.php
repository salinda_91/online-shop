<?php

$products = get_latest_products($con);

include 'blocks/header.php'; ?>

<!-- Start slider -->
<section id="aa-slider">
    <div class="aa-slider-area">
        <div id="sequence" class="seq">
            <div class="seq-screen">
                <ul class="seq-canvas">
                    <!-- single slide item -->
                    <li>
                        <div class="seq-model">
                            <img data-seq src="<?php echo $site_url; ?>views/assets/img/bluedress.jpg" alt=""/>
                        </div>
                        <div class="seq-title">
                            <h2 data-seq>WOMEN COLLECTION</h2>
                            <a data-seq href="#" class="aa-shop-now-btn aa-secondary-btn">SHOP NOW</a>
                        </div>
                    </li>
                    <!-- single slide item -->
                    <li>
                        <div class="seq-model">
                            <img data-seq src="<?php echo $site_url; ?>views/assets/img/img1.jpg" s alt=""/>
                        </div>
                        <div class="seq-title">

                            <h2 data-seq>MEN COLLECTION</h2>

                            <a data-seq href="#" class="aa-shop-now-btn aa-secondary-btn">SHOP NOW</a>
                        </div>
                    </li>
                    <!-- single slide item -->
                    <li>
                        <div class="seq-model">
                            <img data-seq src="<?php echo $site_url; ?>views/assets/img/linenD.jpg" alt=""/>
                        </div>
                        <div class="seq-title">

                            <h2 data-seq>LINEN COLLECTION</h2>

                            <a data-seq href="#" class="aa-shop-now-btn aa-secondary-btn">SHOP NOW</a>
                        </div>
                    </li>
                    <!-- single slide item -->
                    <li>
                        <div class="seq-model">
                            <img data-seq src="assets/img/offers.jpg" alt=""/>
                        </div>
                        <div class="seq-title">

                            <h2 data-seq>EXCLUSIVE OFFERS</h2>

                            <a data-seq href="#" class="aa-shop-now-btn aa-secondary-btn">SHOP NOW</a>
                        </div>
                    </li>
                    <!-- single slide item -->

                </ul>
            </div>
            <!-- slider navigation btn -->
            <fieldset class="seq-nav" aria-controls="sequence" aria-label="Slider buttons">
                <a type="button" class="seq-prev" aria-label="Previous"><span class="fa fa-angle-left"></span></a>
                <a type="button" class="seq-next" aria-label="Next"><span class="fa fa-angle-right"></span></a>
            </fieldset>
        </div>
    </div>
</section>
<!-- / slider -->
<!-- Start Promo section -->


<!-- / Promo section -->
<!-- Products section -->
<section id="aa-product">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="aa-product-area">
                        <div class="aa-product-inner">
                            <!-- start prduct navigation -->
                            <h4 class="home-latest">Latest Products</h4>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <!-- Start men product category -->
                                <div class="tab-pane fade in active" id="men">
                                    <ul class="aa-product-catg">
                                        <!-- start single product item -->
                                        <?php foreach ($products as $product) {
                                            $image = explode(',', $product['images']);
                                            ?>
                                            <li>
                                                <figure>
                                                    <a class="aa-product-img"
                                                       href="<?php echo $site_url; ?>product.php?product=<?php echo $product['id']; ?>">
                                                        <img src="<?php echo $site_url; ?>uploads/product_<?php echo $product['id']; ?>/<?php echo $image[0]; ?>"
                                                             alt="polo shirt img" width="250" height="300">
                                                    </a>
                                                    <form action="<?php echo $site_url; ?>home.php?product=<?php echo $product['id']; ?>&&action=add"
                                                          method="post">
                                                        <input type="hidden" min="1" value="1" name="quantity" id="qty">
                                                        <button class="aa-add-card-btn btn btn-block"
                                                                type="submit" <?php echo ($product['available_qty'] > 0) ? '' : 'disabled' ?>>
                                                            <span class="fa fa-shopping-cart"></span>Add
                                                            To Cart
                                                        </button>
                                                    </form>
                                                    <figcaption>
                                                        <h4 class="aa-product-title"><a
                                                                    href="<?php echo $site_url; ?>product.php?product=<?php echo $product['id']; ?>">
                                                                <?php echo $product['title']; ?>
                                                            </a>
                                                        </h4>
                                                        <span class="aa-product-price">LKR <?php echo $product['unit_price']; ?></span>
                                                    </figcaption>
                                                </figure>
                                                <div class="aa-product-hvr-content">
                                                </div>
                                                <!-- product badge -->
                                                <?php if ($product['available_qty'] > 0) { ?>

                                                <?php } else { ?>
                                                    <span class="aa-badge aa-sold-out" href="#">Sold Out</span>
                                                <?php } ?>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                    <a class="aa-browse-btn" href="<?php echo $site_url; ?>product-list.php">Browse all Product <span
                                                class="fa fa-long-arrow-right"></span></a></div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- / Products section -->
<!-- banner section -->

<!-- popular section -->

</ul>

</div>
<!-- / latest product category -->
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<!-- / popular section -->
<!-- Support section -->
<section id="aa-support">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="aa-support-area">
                    <!-- single support -->
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="aa-support-single">
                            <span class="fa fa-truck"></span>
                            <h4>FREE DELIVERY</h4>
                            <P>Free delivery on all orders over Rs.10000.00</P>
                        </div>
                        s
                    </div>
                    <!-- single support -->
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="aa-support-single">
                            <span class="fa fa-clock-o"></span>
                            <h4>30 DAYS MONEY BACK</h4>
                            <P>30 days money back guarantee only for the items you buy online</P>
                        </div>
                    </div>
                    <!-- single support -->
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="aa-support-single">
                            <span class="fa fa-phone"></span>
                            <h4>SUPPORT 24/7</h4>
                            <P>Contact us for more details</P>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- / Support section -->
<!-- Testimonial -->
<section id="aa-testimonial">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="aa-testimonial-area">
                    <ul class="aa-testimonial-slider">
                        <!-- single slide -->
                        <li>
                            <div class="aa-testimonial-single"><span
                                        class="fa fa-quote-left aa-testimonial-quote"></span>
                                <p>Shop online, so when you go home there's a gift from you, to you.</p>

                                <div class="aa-testimonial-info">
                                    <p>Allison</p>
                                    <span>Designer</span>
                                    <a href="#">Dribble.com</a>
                                </div>
                            </div>
                        </li>
                        <!-- single slide -->
                        <li>
                            <div class="aa-testimonial-single">
                                <img class="aa-testimonial-img" src="<?php echo $site_url; ?>views/assets/img/CEO.png"
                                     alt="testimonial img">
                                <span class="fa fa-quote-left aa-testimonial-quote"></span>
                                <p>Shop online, so when you go home there's a gift from you, to you.</p>
                                <div class="aa-testimonial-info">
                                    <p>Dilshan Witharana</p>
                                    <span>CEO</span>
                                    <a href="#">DMD Fashions</a>
                                </div>
                            </div>
                        </li>
                        <!-- single slide -->
                        <li>
                            <div class="aa-testimonial-single">
                                <img class="aa-testimonial-img"
                                     src="<?php echo $site_url; ?>views/assets/img/testimonial-img-3.jpg"
                                     alt="testimonial img">
                                <span class="fa fa-quote-left aa-testimonial-quote"></span>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt distinctio omnis
                                    possimus, facere, quidem qui!consectetur adipisicing elit. Sunt distinctio omnis
                                    possimus, facere, quidem qui.</p>
                                <div class="aa-testimonial-info">
                                    <p>Luner</p>
                                    <span>COO</span>
                                    <a href="#">Kinatic Solution</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- / Testimonial -->


<?php include 'blocks/footer.php'; ?>

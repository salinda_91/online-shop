<?php include 'blocks/header.php'; ?>
    <!-- catg header banner section -->
    <section id="aa-catg-head-banner">
        <img src="<?php echo $site_url; ?>views/assets/img/fashion/fashion-header-bg-8.jpg" alt="fashion img">
        <div class="aa-catg-head-banner-area">
            <div class="container">
                <div class="aa-catg-head-banner-content">
                    <h2>Checkout Page</h2>
                    <ol class="breadcrumb">
                        <li><a href="index.html">Home</a></li>
                        <li class="active">Checkout</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!-- / catg header banner section -->

    <!-- Cart view section -->
    <section id="checkout">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="checkout-area">
<!--                        <form action="">-->
                            <div class="row">
                                <form action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="post">
                                    <div class="col-md-8">
                                        <div class="checkout-left">
                                            <div class="panel-group" id="accordion">
                                                <!-- Coupon section -->
                                                <div class="panel panel-default aa-checkout-coupon">
                                                    <div id="collapseOne" class="panel-collapse collapse in">

                                                    </div>
                                                </div>
                                                <!-- Login section -->
                                                <div class="panel panel-default aa-checkout-login">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion"
                                                               href="#collapseTwo">
                                                                Client Login
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseTwo"
                                                         class="panel-collapse collapse <?php echo (!$is_loged) ? 'in' : ''; ?>">
                                                        <div class="panel-body">
                                                            <?php
                                                            if ($is_loged) {
                                                                ?>
                                                                <p>Welcome <?php echo ucfirst($_SESSION['name']); ?>,
                                                                    You have already login.</p>
                                                            <?php } else { ?>
                                                                <a href="<?php echo $site_url; ?>login.php?return=checkout.php"
                                                                   class="aa-browse-btn btn-block text-center">Login</a>
                                                                <br>
                                                                <br>
                                                                <p class="text-center"><b>Login to continue.</b></p>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Billing Details -->
                                                <div class="panel panel-default aa-checkout-billaddress">
                                                    <div class="panel-heading <?php echo (!$is_loged) ? 'disabled_click' : ''; ?>">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion"
                                                               href="#collapseThree">
                                                                Billing Details
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseThree"
                                                         class="panel-collapse collapse <?php echo ($is_loged) ? 'in' : ''; ?>">
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="aa-checkout-single-bill">
                                                                        <input type="text" placeholder="Name" required
                                                                               name="name" value="<?php echo $name; ?>" minlength="3">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="aa-checkout-single-bill">
                                                                        <input type="tel" placeholder="Phone" required
                                                                               name="phone"
                                                                               value="<?php echo $phone; ?>" maxlength="10" minlength="10"
                                                                               onkeypress="return isNumber(event)">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="aa-checkout-single-bill">
                                                                        <input type="email" placeholder="Email" required
                                                                               name="email"
                                                                               value="<?php echo $email; ?>" minlength="3" maxlength="100">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="aa-checkout-single-bill">
                                                                        <input type="text" placeholder="Postal Code"
                                                                               name="postalcode"
                                                                               value="<?php echo $postalcode; ?>" maxlength="6" minlength="3"
                                                                               onkeypress="return isNumber(event)">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="aa-checkout-single-bill">
                                                                        <input type="text" placeholder="City / Town"
                                                                               name="city" value="<?php echo $city; ?>" minlength="3">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="aa-checkout-single-bill">
                                                                        <textarea placeholder="Address"
                                                                                  name="address"><?php echo $address; ?></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="checkout-right">
                                            <h4>Order Summary</h4>
                                            <div class="aa-order-summary-area">
                                                <table class="table table-responsive">
                                                    <thead>
                                                    <tr>
                                                        <th>Product</th>
                                                        <th>Total</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    $total_quantity = 0;
                                                    $total_price = 0;
                                                    if (isset($_SESSION["cart_item"])) {

                                                        foreach ($_SESSION["cart_item"] as $item) {
                                                            $item_price = $item["quantity"] * $item["price"];
                                                            $image = explode(',', $item['images']);
                                                            ?>
                                                            <tr>
                                                                <td><?php echo $item['title']; ?> <strong>
                                                                        x <?php echo $item["quantity"]; ?></strong></td>
                                                                <td><b>LKR <?php echo $item_price ?></b></td>
                                                            </tr>
                                                            <?php
                                                            $total_quantity += $item["quantity"];
                                                            $total_price += ($item["price"] * $item["quantity"]);
                                                        }
                                                    }
                                                    ?>
                                                    <input type="hidden" name="total_quantity" value="<?php echo $total_quantity;?>">
                                                    <input type="hidden" name="total_price" value="<?php echo $total_price;?>">
                                                    </tbody>
                                                    <tfoot style="    background: #eee;">
                                                    <tr>
                                                        <th>Items</th>
                                                        <td><?php echo $total_quantity; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Total</th>
                                                        <td>LKR <?php echo $total_price; ?></td>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                            <h4>Payment Method</h4>
                                            <div class="aa-payment-method">
                                                <label for="cashdelivery"><input type="radio" id="cashdelivery"
                                                                                 name="optionsRadios"> Cash on Delivery
                                                </label>
                                                <!--                    <label for="paypal"><input type="radio" id="paypal" name="optionsRadios" checked> Via Paypal </label>-->
                                                <!--                    <img src="https://www.paypalobjects.com/webstatic/mktg/logo/AM_mc_vs_dc_ae.jpg" border="0" alt="PayPal Acceptance Mark">    -->
                                                <input type="submit" value="Place Order" class="aa-browse-btn">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
<!--                        </form>-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- / Cart view section -->

<?php include 'blocks/footer.php'; ?>
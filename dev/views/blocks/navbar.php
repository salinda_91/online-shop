<section id="menu">
    <div class="container">
        <div class="menu-area">
            <!-- Navbar -->
            <div class="navbar navbar-default" role="navigation">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="navbar-collapse collapse">
                    <!-- Left nav -->
                    <ul class="nav navbar-nav">
                        <li><a href="<?php echo $site_url; ?>home.php">Home</a></li>
                        <li><a href="<?php echo $site_url; ?>product-list.php?category=1">Men <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo $site_url; ?>product-list.php?category=1&sub_category=1">T shirts</a></li>
                                <li><a href="<?php echo $site_url; ?>product-list.php?category=1&sub_category=1">Shirts</a></li>
                                <li><a href="<?php echo $site_url; ?>product-list.php?category=1&sub_category=1">Shorts</a></li>
                            </ul>
                        </li>
                        <li><a href="<?php echo $site_url; ?>product-list.php?category=2">Women <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo $site_url; ?>product-list.php?category=2&sub_category=4">Tops</a></li>
                                <li><a href="<?php echo $site_url; ?>product-list.php?category=2&sub_category=5">Pants</a></li>
                                <li><a href="<?php echo $site_url; ?>product-list.php?category=2&sub_category=6">Dresses<span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo $site_url; ?>product-list.php?category=2&sub_category=5&sub_sub_category=1">Casual dresses</a></li>
                                        <li><a href="<?php echo $site_url; ?>product-list.php?category=2&sub_category=5&sub_sub_category=2">Party dresses</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li><a href="<?php echo $site_url; ?>product-list.php?category=3">Unisex </a></li>


                        <li><a href="<?php echo $site_url; ?>/contact.php">Contact</a></li>

                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>
    </div>
</section>
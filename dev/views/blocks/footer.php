<!-- footer -->
<footer id="aa-footer">
    <!-- footer bottom -->
    <div class="aa-footer-top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="aa-footer-top-area">
                        <div class="row">
                            <div class="col-md-3 col-sm-6">
                                <div class="aa-footer-widget">
                                    <h3>Main Menu</h3>
                                    <ul class="aa-footer-nav">
                                        <li><a href="#">Home</a></li>
                                        <li><a href="#">Our Products</a></li>
                                        <li><a href="#">About Us</a></li>
                                        <li><a href="#">Contact Us</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="aa-footer-widget">
                                    <div class="aa-footer-widget">
                                        <h3>Knowledge Base</h3>
                                        <ul class="aa-footer-nav">
                                            <li><a href="#">Delivery</a></li>
                                            <li><a href="#">Returns</a></li>
                                            <li><a href="#">Discount</a></li>
                                            <li><a href="#">Special Offer</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="aa-footer-widget">
                                    <div class="aa-footer-widget">
                                        <h3>Useful Links</h3>
                                        <ul class="aa-footer-nav">
                                            <li><a href="#">Site Map</a></li>
                                            <li><a href="#">Search</a></li>
                                            <li><a href="#">FAQ</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="aa-footer-widget">
                                    <div class="aa-footer-widget">
                                        <h3>Contact Us</h3>
                                        <address>
                                            <p> 117 KATUGASTHOTA ROAD, KANDY 10003, SRI LANKA</p>
                                            <p><span class="fa fa-phone"></span>+94 702-955-782</p>
                                            <p><span class="fa fa-envelope"></span>dilshan@dmdtravels.com</p>
                                        </address>
                                        <div class="aa-footer-social">
                                            <a href="#"><span class="fa fa-facebook"></span></a>
                                            <a href="#"><span class="fa fa-twitter"></span></a>
                                            <a href="#"><span class="fa fa-google-plus"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer-bottom -->
    <div class="aa-footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="aa-footer-bottom-area">

                        <!--<div class="aa-footer-payment">
                          //<span class="fa fa-cc-mastercard"></span>
            //              <span class="fa fa-cc-visa"></span>
            //              <span class="fa fa-paypal"></span>
            //              <span class="fa fa-cc-discover"></span>
                        </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- / footer -->

<!-- Login Modal -->
<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4>Login or Register</h4>
                <form class="aa-login-form" action="">
                    <label for="">Username or Email address<span>*</span></label>
                    <input type="text" placeholder="Username or email">
                    <label for="">Password<span>*</span></label>
                    <input type="password" placeholder="Password">
                    <button class="aa-browse-btn" type="submit">Login</button>
                    <label for="rememberme" class="rememberme"><input type="checkbox" id="rememberme"> Remember me
                    </label>
                    <p class="aa-lost-password"><a href="#">Lost your password?</a></p>
                    <div class="aa-register-now">
                        Don't have an account?<a href="account.html">Register now!</a>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo $site_url; ?>views/assets/js/bootstrap.js"></script>
<!-- SmartMenus jQuery plugin -->
<script type="text/javascript" src="<?php echo $site_url; ?>views/assets/js/jquery.smartmenus.js"></script>
<!-- SmartMenus jQuery Bootstrap Addon -->
<script type="text/javascript" src="<?php echo $site_url; ?>views/assets/js/jquery.smartmenus.bootstrap.js"></script>
<!-- To Slider JS -->
<script src="<?php echo $site_url; ?>views/assets/js/sequence.js"></script>
<script src="<?php echo $site_url; ?>views/assets/js/sequence-theme.modern-slide-in.js"></script>
<!-- Product view slider -->
<script type="text/javascript" src="<?php echo $site_url; ?>views/assets/js/jquery.simpleGallery.js"></script>
<script type="text/javascript" src="<?php echo $site_url; ?>views/assets/js/jquery.simpleLens.js"></script>
<!-- slick slider -->
<script type="text/javascript" src="<?php echo $site_url; ?>views/assets/js/slick.js"></script>
<!-- Price picker slider -->
<script type="text/javascript" src="<?php echo $site_url; ?>views/assets/js/nouislider.js"></script>
<!-- Custom js -->
<script src="<?php echo $site_url; ?>views/assets/js/custom.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

<script>
    var site_url = "<?php echo $site_url; ?>";
    <?php
    echo $success;
    echo $reg_success;
    echo $order_success;
    echo $success_order;

    ?>
    function alert_msg() {
        $.alert({
            icon: 'fa fa-smile-o',
            theme: 'modern',
            title: 'Success!',
            content: 'Item added to cart successfully!',
            closeIcon: true,
            animation: 'scale',
            type: 'green',
            buttons: {
                somethingElse: {
                    text: 'Continue Shopping',
                    btnClass: 'btn-blue',
                    keys: ['enter', 'shift'],
                    action: function () {
                        window.location = site_url + 'product-list.php';
                    }
                }
            }
        });
    }

    function register_success() {
        $.alert({
            icon: 'fa fa-smile-o',
            theme: 'modern',
            title: 'Register Success!',
            content: 'User registered successfully. You will redirect to Login page automatically.',
            closeIcon: true,
            animation: 'scale',
            type: 'green',
            buttons: []
        });
        setTimeout(function () {
            window.location = site_url + 'login.php';
        }, 2500)
    }

    function order_success() {
        $.alert({
            icon: 'fa fa-smile-o',
            theme: 'modern',
            title: 'Order Success!',
            content: 'You have placed the order successfully. Check order details on your profile.',
            closeIcon: true,
            animation: 'scale',
            type: 'green',
            buttons: {
                cancel: function () {
                }
            }
        });

        setTimeout(function () {
            window.location = site_url + 'my-account.php';
        }, 2500)
    }

    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    //-----------
    $.fn.extend({
        treeview: function () {
            return this.each(function () {
                // Initialize the top levels;
                var tree = $(this);

                tree.addClass('treeview-tree');
                tree.find('li').each(function () {
                    var stick = $(this);
                    $(this).prepend("<i class='tree-indicator glyphicon glyphicon-chevron-right'></i>");
                });
                tree.find('li').has("ul").each(function () {
                    var branch = $(this); //li with children ul

                    // branch.prepend("<i class='tree-indicator glyphicon glyphicon-chevron-right'></i>");
                    branch.addClass('tree-branch');
                    branch.on('click', function (e) {
                        if (this == e.target) {
                            var icon = $(this).children('i:first');

                            icon.toggleClass("glyphicon-chevron-down glyphicon-chevron-right");
                            $(this).children().children().toggle();
                        }
                    })
                    branch.children().children().toggle();

                    /**
                     *    The following snippet of code enables the treeview to
                     *    function when a button, indicator or anchor is clicked.
                     *
                     *    It also prevents the default function of an anchor and
                     *    a button from firing.
                     */
                    branch.children('.tree-indicator, button, a').click(function (e) {
                        branch.click();

                        e.preventDefault();
                    });
                });
            });
        }
    });

    /**
     *    The following snippet of code automatically converst
     *    any '.treeview' DOM elements into a treeview component.
     */
    $(window).on('load', function () {
        $('.treeview').each(function () {
            var tree = $(this);
            tree.treeview();
        });

    })
</script>

</body>
</html>
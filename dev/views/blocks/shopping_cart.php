<!-- cart box -->
<div class="aa-cartbox">
    <a class="aa-cart-link" href="<?php echo $site_url; ?>shopping-cart.php">
        <span class="fa fa-shopping-basket"></span>
        <span class="aa-cart-title">SHOPPING CART</span>
        <?php
        $item_count=0;
        if (isset($_SESSION["cart_item"])) {
            $item_count = count($_SESSION["cart_item"]);
        }
        ?>
        <span class="aa-cart-notify"><?php echo $item_count; ?></span>
    </a>
    <div class="aa-cartbox-summary">
        <ul>
            <?php
            if (isset($_SESSION["cart_item"])) {
                $total_quantity = 0;
                $total_price = 0;
                foreach ($_SESSION["cart_item"] as $item) {
                    $item_price = $item["quantity"] * $item["price"];
                    $image = explode(',', $item['images']);
                    ?>
                    <li>
                        <a class="aa-cartbox-img" href="#"><img
                                    src="<?php echo $site_url; ?>uploads/product_<?php echo $item['id']; ?>/<?php echo $image[0]; ?>"
                                    alt="img"></a>
                        <div class="aa-cartbox-info">
                            <h4><a href="<?php echo $site_url; ?>product.php?product=<?php echo $item['id']; ?>"><?php echo $item['title']; ?></a></h4>
                            <p><?php echo $item["quantity"]; ?> x LKR <?php echo $item["price"]; ?></p>
                            <p><b>LKR <?php echo $item_price ?></b></p>
                        </div>
                        <?php
                        $uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
                        $url = 'http://' . $_SERVER['HTTP_HOST'] . $uri_parts[0];
                        ?>
                        <a class="aa-remove-product" href="<?php echo $url; ?>?product=<?php echo $item["id"]; ?>&action=remove">
                            <span class="fa fa-times"></span></a>
                    </li>
                    <?php
                    $total_quantity += $item["quantity"];
                    $total_price += ($item["price"] * $item["quantity"]);
                }
                ?>
                <li>
                    <span class="aa-cartbox-total-title">Items</span>
                    <span class="aa-cartbox-total-price"><?php echo $total_quantity; ?></span>
                </li>
                <li>
                    <span class="aa-cartbox-total-title">Total</span>
                    <span class="aa-cartbox-total-price"><?php echo $total_price; ?></span>
                </li>
                <a class="aa-cartbox-checkout aa-primary-btn" href="<?php echo $site_url; ?>shopping-cart.php">Checkout</a>
                <a class="aa-cartbox-checkout aa-primary-btn empty-cart" href="<?php echo $url; ?>?product=<?php echo $item["id"]; ?>&action=empty">Empty Cart</a>
                <?php
            }else{
                ?>
                <li>
                    <span class="aa-cartbox-total-title">No items selected</span>
                </li>
                <a class="aa-cartbox-checkout aa-primary-btn" href="<?php echo $site_url; ?>product-list.php">Continue Shopping</a>
             <?php

            }
            ?>


        </ul>

    </div>
</div>
<!-- / cart box -->
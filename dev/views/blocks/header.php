<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Daily Shop | Home</title>

    <!-- Font awesome -->
    <link href="<?php echo $site_url; ?>views/assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="<?php echo $site_url; ?>views/assets/css/bootstrap.css" rel="stylesheet">
    <!-- SmartMenus jQuery Bootstrap Addon CSS -->
    <link href="<?php echo $site_url; ?>views/assets/css/jquery.smartmenus.bootstrap.css" rel="stylesheet">
    <!-- Product view slider -->
    <link rel="stylesheet" type="text/css" href="<?php echo $site_url; ?>views/assets/css/jquery.simpleLens.css">
    <!-- slick slider -->
    <link rel="stylesheet" type="text/css" href="<?php echo $site_url; ?>views/assets/css/slick.css">
    <!-- price picker slider -->
    <link rel="stylesheet" type="text/css" href="<?php echo $site_url; ?>views/assets/css/nouislider.css">
    <!-- Theme color -->
    <link id="switcher" href="<?php echo $site_url; ?>views/assets/css/theme-color/default-theme.css" rel="stylesheet">
    <!-- <link id="switcher" href="css/theme-color/bridge-theme.css" rel="stylesheet"> -->
    <!-- Top Slider CSS -->
    <link href="<?php echo $site_url; ?>views/assets/css/sequence-theme.modern-slide-in.css" rel="stylesheet"
          media="all">

    <!-- Main style sheet -->
    <link href="<?php echo $site_url; ?>views/assets/css/style.css" rel="stylesheet">

    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesnt work if you view the page via file-->
    <!--[if lt IE 9]-->
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <![endif]-->


</head>
<body>
<!-- wpf loader Two -->
<!--    <div id="wpf-loader-two">-->
<!--      <div class="wpf-loader-two-inner">-->
<!--        <span>Loading</span>-->
<!--      </div>-->
<!--    </div>-->
<!-- / wpf loader Two -->

<!-- SCROLL TOP BUTTON -->
<a class="scrollToTop" href="#"><i class="fa fa-chevron-up"></i></a>
<!-- END SCROLL TOP BUTTON -->


<!-- Start header section -->
<header id="aa-header">
    <!-- start header top  -->
    <div class="aa-header-top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="aa-header-top-area">
                        <!-- start header top left -->
                        <div class="aa-header-top-left">
                            <!-- start language -->

                            <!-- / language -->

                            <!-- start currency -->
                            <div class="aa-currency">
                                <div class="dropdown">

                                    <!--                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">-->
                                    <!--                                        <li><a href="#"><i class="fa fa-euro"></i>EURO</a></li>-->
                                    <!--                                        <li><a href="#"><i class="fa fa-jpy"></i>YEN</a></li>-->
                                    <!--                                    </ul>-->
                                </div>
                            </div>
                            <!-- / currency -->
                            <!-- start cellphone -->
                            <div class="cellphone hidden-xs">
                                <p><span class="fa fa-phone"></span>00-94-702-955-782</p>
                            </div>
                            <!-- / cellphone -->
                        </div>
                        <!-- / header top left -->
                        <div class="aa-header-top-right">
                            <ul class="aa-head-top-nav-right">

                                <li class="hidden-xs"><a href="<?php echo $site_url; ?>shopping-cart.php">My Cart</a>
                                </li>
                                <?php
                                //                                var_dump($_SESSION);
                                if (isset($_SESSION['user_id']) && isset($_SESSION['email'])) {
                                    ?>
                                    <li><a href="<?php echo $site_url; ?>my-account.php">Welcome, <?php echo ucfirst($_SESSION['name']) ?></a></li>
                                    <li><a href="<?php echo $site_url; ?>logout.php">Logout</a></li>
                                <?php } else { ?>
                                    <li class=""><a href="<?php echo $site_url; ?>login.php">Login</a></li>
                                <?php } ?>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- / header top  -->

    <!-- start header bottom  -->
    <div class="aa-header-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="aa-header-bottom-area">
                        <!-- logo  -->
                        <div class="aa-logo">
                            <!-- Text based logo -->
                            <a href="<?php echo $site_url; ?>">
                                <!--<span class="fa fa-shopping-cart"></span>

                                  <!--<p>daily<strong>Shop</strong> <span>Your Shopping Partner</span></p>-->
                            </a>
                            <!-- img based logo -->
                            <a href="<?php echo $site_url; ?>"><img src="<?php echo $site_url; ?>views/assets/img/logo-slimncurvy.jpg"
                                                      alt="logo img"></a>
                        </div>
                        <!-- / logo  -->
                        <?php include_once 'shopping_cart.php' ?>
                        <!-- search box -->
                        <div class="aa-search-box">
<!--                            <form action="">-->
<!--                                <input type="text" name="" id="" placeholder="Search here ex. 'man' ">-->
<!--                                <button type="submit"><span class="fa fa-search"></span></button>-->
<!--                            </form>-->
                        </div>
                        <!-- / search box -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- / header bottom  -->
</header>
<!-- / header section -->
<!-- menu -->
<?php include 'navbar.php'; ?>
<!-- / menu -->
   
<?php
include 'blocks/header.php';
?>
    <!-- catg header banner section -->
    <section id="aa-catg-head-banner">
        <img src="<?php echo $site_url; ?>views/assets/img/fashion/fashion-header-bg-8.jpg" alt="fashion img">
        <div class="aa-catg-head-banner-area">
            <div class="container">
                <div class="aa-catg-head-banner-content">
                    <h2>Fashion</h2>
                    <ol class="breadcrumb">
                        <li><a href="index.html">Home</a></li>
                        <li class="active">Women</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!-- / catg header banner section -->

    <!-- product category -->
    <section id="aa-product-category">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-8 col-md-push-3">
                    <div class="aa-product-catg-content">
                        <div class="aa-product-catg-body">
                            <div class="breadcrumb">
                                <p style="margin: 5px 0px">
                                    <span><a href="<?php echo $site_url; ?>">Home&nbsp;&nbsp;></a></span>
                                    <span><a href="<?php echo $site_url; ?>product-list.php">Products&nbsp;&nbsp;></a></span>
                                    <?php if ($category) { ?>
                                        <span><a href="#"><?php echo ucfirst(get_category_name($con, $category, 'category')); ?>&nbsp;&nbsp;></a></span>
                                    <?php } ?>
                                    <?php if ($sub_category) { ?>
                                        <span><a href="#"><?php echo ucfirst(get_category_name($con, $sub_category, 'sub_category')); ?>&nbsp;&nbsp;></a></span>
                                    <?php } ?>
                                    <?php if ($sub_sub_category) { ?>
                                        <span><a href="#"><?php echo ucfirst(get_category_name($con, $sub_sub_category, 'sub_sub_category')); ?>&nbsp;&nbsp;></a></span>
                                    <?php } ?>
                                </p>
                            </div>
                            <ul class="aa-product-catg">
                                <!-- start single product item -->
                                <?php
                                if (count($products)) {
                                    foreach ($products as $product) {
                                        $image = explode(',', $product['images']);
                                        ?>
                                        <li>
                                            <figure>
                                                <a class="aa-product-img"
                                                   href="<?php echo $site_url; ?>product.php?product=<?php echo $product['id']; ?>">
                                                    <img src="<?php echo $site_url; ?>uploads/product_<?php echo $product['id']; ?>/<?php echo $image[0]; ?>"
                                                         alt="polo shirt img"></a>
                                                <form action="<?php echo $site_url; ?>product-list.php?product=<?php echo $product['id']; ?>&&action=add"
                                                      method="post">
                                                    <input type="hidden" min="1" value="1" name="quantity" id="qty">
                                                    <button class="aa-add-card-btn btn btn-block"
                                                            type="submit" <?php echo ($product['available_qty'] > 0) ? '' : 'disabled' ?>>
                                                        <span class="fa fa-shopping-cart"></span>Add
                                                        To Cart
                                                    </button>
                                                </form>
                                                <figcaption>
                                                    <h4 class="aa-product-title"><a
                                                                href="#"><?php echo $product['title']; ?></a></h4>
                                                    <span class="aa-product-price">LKR <?php echo $product['unit_price']; ?></span>
                                                    <!--                                                <span class="aa-product-price"><del>$65.50</del></span>-->

                                                </figcaption>
                                            </figure>

                                            <!-- product badge -->
                                            <?php if ($product['available_qty'] > 0) { ?>
                                                <!--                                            <span class="aa-badge aa-sale" href="#">SALE!</span>-->
                                            <?php } else { ?>
                                                <span class="aa-badge aa-sold-out" href="#">Sold Out</span>
                                            <?php } ?>
                                        </li>
                                    <?php }
                                } else { ?>
                                    <li style="width: 100%;">
                                        <h3>No items found.</h3>
                                    </li>
                                <?php } ?>

                            </ul>

                        </div>

                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-4 col-md-pull-9">
                    <aside class="aa-sidebar">
                        <!-- single sidebar -->
                        <div class="aa-sidebar-widget" style="margin-top: 50px;">
                            <h3>Category</h3>
                            <ul class="treeview">
                                <?php foreach ($categories as $cat) {
                                    $sub_categories = get_sub_categories($con, $cat['id']);
//                                    var_dump(count($sub_categories));
                                    if (count($sub_categories)) { ?>
                                        <li><a href="#"><?php echo ucfirst($cat['category_name']); ?></a>
                                            <ul class="<?php echo ($cat['id'] == $category) ? 'active_ul' : '' ?>">
                                                <?php foreach ($sub_categories as $sub) {
                                                    $sub_sub_categories = get_sub_sub_categories($con, $cat['id'], $sub['id']);
//                                                    var_dump($cat['id'].$sub['id']);
                                                    if (count($sub_sub_categories)) {
                                                        ?>
                                                        <li>
                                                            <a href="#"><?php echo ucfirst($sub['sub_category_name']); ?></a>
                                                            <ul class="<?php echo ($sub['id'] == $sub_category) ? 'active_ul' : '' ?>">
                                                                <?php foreach ($sub_sub_categories as $sub_sub) { ?>
                                                                    <li>
                                                                        <a href="<?php echo $site_url; ?>product-list.php?category=<?php echo $cat['id']; ?>&sub_category=<?php echo $sub['id']; ?>&sub_sub_category=<?php echo $sub_sub['id']; ?>"><?php echo ucfirst($sub_sub['sub_sub_category_name']); ?></a>
                                                                    </li>
                                                                <?php } ?>
                                                            </ul>
                                                        </li>
                                                    <?php } else { ?>
                                                        <li>
                                                            <a href="<?php echo $site_url; ?>product-list.php?category=<?php echo $cat['id']; ?>&sub_category=<?php echo $sub['id']; ?>"><?php echo ucfirst($sub['sub_category_name']); ?></a>
                                                        </li>
                                                    <?php } ?>
                                                <?php } ?>
                                            </ul>
                                        </li>
                                    <?php } else { ?>
                                        <li>
                                            <a href="<?php echo $site_url; ?>product-list.php?category=<?php echo $cat['id']; ?>"><?php echo ucfirst($cat['category_name']); ?></a>
                                        </li>
                                    <?php } ?>

                                <?php } ?>
                            </ul>

                        </div>

                        <!-- single sidebar -->

                    </aside>
                </div>

            </div>
        </div>
    </section>
    <!-- / product category -->

<?php
include 'blocks/footer.php';
?>
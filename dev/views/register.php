 <?php include 'blocks/header.php';?>
 
  <!-- catg header banner section -->
  <section id="aa-catg-head-banner">
      <img src="<?php echo $site_url; ?>views/assets/img/fashion/fashion-header-bg-8.jpg" alt="fashion img">
      <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <h2>Register</h2>
        <ol class="breadcrumb">
          <li><a href="<?php echo $site_url; ?>">Home</a></li>
          <li class="active">Register</li>
        </ol>
      </div>
     </div>
   </div>
  </section>
  <!-- / catg header banner section -->

 <!-- Cart view section -->
 <section id="aa-myaccount">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
        <div class="aa-myaccount-area">         
            <div class="row">
     
              <div class="col-md-6 col-md-offset-3">
                <div class="aa-myaccount-register">                 
                 <h4>Register</h4>
                  <p class="er"><?php include('config/errors.php'); ?></p>

                 <form  class="aa-login-form"  method="post" action="register.php">               

                    
                    <label for="">Name<span>*</span></label>
                    <input type="text" placeholder="Name" name="username" value="<?php echo $username;?>">

                    <label for="">Email Address<span>*</span></label>
                    <input type="text" placeholder="Email" name="email" value="<?php echo $email;?>">

                    <label for="">Password<span>*</span></label>
                    <input type="password" placeholder="Password" name="password_1">

                    <label for="">Confirm Password<span>*</span></label>
                    <input type="password" placeholder="Password" name= "password_2">

                    <button type="submit" name="reg_user" class="aa-browse-btn">Register</button>                    
                  </form>
                    

                </div>
              </div>
            </div>          
         </div>
       </div>
     </div>
   </div>
 </section>
 <!-- / Cart view section -->

<?php include 'blocks/footer.php';?>
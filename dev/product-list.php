<?php
session_start();
include_once "config/config.php";
include_once "config/function.php";
require "add_to_cart.php";

$cat = null;
$sub_cat = null;
$category = null;
$sub_category = null;
$sub_sub_category= null;

$categories = get_categories($con);

if (isset($_GET['category'])){
    $category= $_GET['category'];
}
if (isset($_GET['sub_category'])){
    $sub_category= $_GET['sub_category'];
}
if (isset($_GET['sub_sub_category'])){
    $sub_sub_category= $_GET['sub_sub_category'];
}
$products = filter_products($con, $category, $sub_category, $sub_sub_category);
include_once "views/product.php";
?>
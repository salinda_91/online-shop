-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 18, 2019 at 06:34 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `online_shop`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`, `status`, `created_at`) VALUES
(1, 'Men', 1, '2019-03-17 11:40:24'),
(2, 'Women', 1, '2019-03-17 11:40:24'),
(3, 'Unisex', 1, '2019-03-17 11:40:39');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `company` varchar(100) DEFAULT NULL,
  `message` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `name`, `subject`, `email`, `company`, `message`) VALUES
(1, 'madawa deshan katuwawala', 'zzzz', 'madawadeshan@gmail.com', 'aaaa', 'aaaa'),
(2, 'madawa deshan katuwawala', 'zzzz', 'madawadeshan@gmail.com', 'aaaa', 'aaaa');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_details_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` double NOT NULL,
  `product_details` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `ordered_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `user_details_id`, `qty`, `price`, `product_details`, `status`, `ordered_at`) VALUES
(1, 4, 1, 3, 3900, '[{\"id\":\"2\",\"title\":\"T-Shirt 2\",\"images\":\"1.jpg,2.jpg,3.jpg\",\"quantity\":2,\"price\":\"1200\"},{\"id\":\"1\",\"title\":\"T-Shirt\",\"images\":\"1.jpg,2.jpg,3.jpg\",\"quantity\":\"1\",\"price\":\"1500\"}]', 1, '2019-03-17 10:50:28'),
(2, 4, 1, 3, 3900, '[{\"id\":\"2\",\"title\":\"T-Shirt 2\",\"images\":\"1.jpg,2.jpg,3.jpg\",\"quantity\":2,\"price\":\"1200\"},{\"id\":\"1\",\"title\":\"T-Shirt\",\"images\":\"1.jpg,2.jpg,3.jpg\",\"quantity\":\"1\",\"price\":\"1500\"}]', 1, '2019-03-17 11:02:24'),
(3, 4, 1, 4, 6000, '{\"1\":{\"id\":\"1\",\"title\":\"T-Shirt\",\"images\":\"1.jpg,2.jpg,3.jpg\",\"quantity\":4,\"price\":\"1500\"}}', 1, '2019-03-17 11:07:55'),
(5, 4, 1, 1, 1200, '{\"2\":{\"id\":\"2\",\"title\":\"T-Shirt 2\",\"images\":\"1.jpg,2.jpg,3.jpg\",\"quantity\":\"1\",\"price\":\"1200\"}}', 1, '2019-03-17 11:13:20'),
(8, 4, 2, 3, 4500, '{\"1\":{\"id\":\"1\",\"title\":\"T-Shirt\",\"images\":\"1.jpg,2.jpg,3.jpg\",\"quantity\":3,\"price\":\"1500\"}}', 1, '2019-03-17 17:58:21');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `title` varchar(100) CHARACTER SET latin1 NOT NULL,
  `images` varchar(255) CHARACTER SET latin1 NOT NULL,
  `unit_price` float NOT NULL,
  `short_description` varchar(255) CHARACTER SET latin1 NOT NULL,
  `long_description` text CHARACTER SET latin1,
  `available_sizes` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `available_colors` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `category` int(11) NOT NULL,
  `sub_category` int(11) DEFAULT NULL,
  `sub_sub_category` int(11) DEFAULT NULL,
  `gender` enum('male','female','unisex') CHARACTER SET latin1 NOT NULL,
  `available_qty` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `images`, `unit_price`, `short_description`, `long_description`, `available_sizes`, `available_colors`, `category`, `sub_category`, `sub_sub_category`, `gender`, `available_qty`, `status`, `created_at`) VALUES
(1, 'T-Shirt', '1.jpg,2.jpg,3.jpg', 1500, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis animi, veritatis quae repudiandae quod nulla porro quidem, itaque quis quaerat!', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Quod, culpa!\r\nLorem ipsum dolor sit amet.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor qui eius esse!\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam, modi!\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Illum, iusto earum voluptates autem esse molestiae ipsam, atque quam amet similique ducimus aliquid voluptate perferendis, distinctio!\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis ea, voluptas! Aliquam facere quas cumque rerum dolore impedit, dicta ducimus repellat dignissimos, fugiat, minima quaerat necessitatibus? Optio adipisci ab, obcaecati, porro unde accusantium facilis repudiandae.', 's,m,l,xl,xxl', '#008000,#222222,#eeeeee', 1, NULL, NULL, 'female', 90, 1, '2019-03-16 14:20:02'),
(2, 'T-Shirt 2', '1.jpg,2.jpg,3.jpg', 1200, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis animi, veritatis quae repudiandae quod nulla porro quidem, itaque quis quaerat!', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Quod, culpa!\r\nLorem ipsum dolor sit amet.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor qui eius esse!\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam, modi!\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Illum, iusto earum voluptates autem esse molestiae ipsam, atque quam amet similique ducimus aliquid voluptate perferendis, distinctio!\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis ea, voluptas! Aliquam facere quas cumque rerum dolore impedit, dicta ducimus repellat dignissimos, fugiat, minima quaerat necessitatibus? Optio adipisci ab, obcaecati, porro unde accusantium facilis repudiandae.', 's,m,l,xl,xxl', '#008000,#222222,#eeeeee', 2, 4, NULL, 'female', 0, 1, '2019-03-16 14:20:07'),
(3, 'T-Shirt 3', '1.jpg,2.jpg,3.jpg', 1200, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis animi, veritatis quae repudiandae quod nulla porro quidem, itaque quis quaerat!', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Quod, culpa!\r\nLorem ipsum dolor sit amet.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor qui eius esse!\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam, modi!\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Illum, iusto earum voluptates autem esse molestiae ipsam, atque quam amet similique ducimus aliquid voluptate perferendis, distinctio!\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis ea, voluptas! Aliquam facere quas cumque rerum dolore impedit, dicta ducimus repellat dignissimos, fugiat, minima quaerat necessitatibus? Optio adipisci ab, obcaecati, porro unde accusantium facilis repudiandae.', 's,m,l,xl,xxl', '#008000,#222222,#eeeeee', 1, NULL, NULL, 'female', 10, 1, '2019-03-16 14:20:07'),
(4, 'T-Shirt 5\r\n', '1.jpg,2.jpg,3.jpg', 1200, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis animi, veritatis quae repudiandae quod nulla porro quidem, itaque quis quaerat!', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Quod, culpa!\r\nLorem ipsum dolor sit amet.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor qui eius esse!\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam, modi!\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Illum, iusto earum voluptates autem esse molestiae ipsam, atque quam amet similique ducimus aliquid voluptate perferendis, distinctio!\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis ea, voluptas! Aliquam facere quas cumque rerum dolore impedit, dicta ducimus repellat dignissimos, fugiat, minima quaerat necessitatibus? Optio adipisci ab, obcaecati, porro unde accusantium facilis repudiandae.', 's,m,l,xl,xxl', '#008000,#222222,#eeeeee', 2, 5, 2, 'female', 10, 1, '2019-03-16 14:20:07'),
(5, 'T-Shirt 6\r\n\r\n', '1.jpg,2.jpg,3.jpg', 1200, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis animi, veritatis quae repudiandae quod nulla porro quidem, itaque quis quaerat!', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Quod, culpa!\r\nLorem ipsum dolor sit amet.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor qui eius esse!\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam, modi!\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Illum, iusto earum voluptates autem esse molestiae ipsam, atque quam amet similique ducimus aliquid voluptate perferendis, distinctio!\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis ea, voluptas! Aliquam facere quas cumque rerum dolore impedit, dicta ducimus repellat dignissimos, fugiat, minima quaerat necessitatibus? Optio adipisci ab, obcaecati, porro unde accusantium facilis repudiandae.', 's,m,l,xl,xxl', '#008000,#222222,#eeeeee', 3, NULL, NULL, 'female', 10, 1, '2019-03-16 14:20:07');

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `sub_category_name` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `category`, `sub_category_name`, `status`) VALUES
(1, 1, 't shirts', 1),
(2, 1, 'shiirts', 1),
(3, 1, 'shorts', 1),
(4, 2, 'tops', 1),
(5, 2, 'pants', 1),
(6, 2, 'dressess', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sub_sub_categories`
--

CREATE TABLE `sub_sub_categories` (
  `id` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `sub_category` int(11) NOT NULL,
  `sub_sub_category_name` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_sub_categories`
--

INSERT INTO `sub_sub_categories` (`id`, `category`, `sub_category`, `sub_sub_category_name`, `status`) VALUES
(1, 2, 5, 'casual dresses', 1),
(2, 2, 5, 'party dresses', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `password`, `status`) VALUES
(4, 'jaya', 'jayawardanasalinda@gmail.com', '4297f44b13955235245b2497399d7a93', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(100) NOT NULL,
  `postalcode` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`id`, `user_id`, `name`, `email`, `phone`, `address`, `city`, `postalcode`, `status`, `created_at`) VALUES
(1, 4, 'salinda jayawardana', 'jayawardanasalinda@gmail.com', '716186394', 'NO88, helweeshiyawaththa, narammala', 'kurunegala', 60100, 1, '2019-03-17 10:36:32'),
(2, 4, 'Dhanushka Salinda Jayawardana', 'jayawardanasalinda@gmail.com', '713882815', 'No 88, 5th Lane\r\nHelweeshiyawaththa', 'Narammala.', 60100, 1, '2019-03-17 17:58:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_sub_categories`
--
ALTER TABLE `sub_sub_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sub_sub_categories`
--
ALTER TABLE `sub_sub_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

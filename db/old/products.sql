-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 16, 2019 at 05:02 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `online_shop`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `title` varchar(100) CHARACTER SET latin1 NOT NULL,
  `images` varchar(255) CHARACTER SET latin1 NOT NULL,
  `unit_price` float NOT NULL,
  `short_description` varchar(255) CHARACTER SET latin1 NOT NULL,
  `long_description` text CHARACTER SET latin1,
  `available_sizes` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `available_colors` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `category` int(11) NOT NULL,
  `sub_category` int(11) DEFAULT NULL,
  `sub_sub_category` int(11) DEFAULT NULL,
  `gender` enum('male','female','unisex') CHARACTER SET latin1 NOT NULL,
  `available_qty` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `images`, `unit_price`, `short_description`, `long_description`, `available_sizes`, `available_colors`, `category`, `sub_category`, `sub_sub_category`, `gender`, `available_qty`, `status`, `created_at`) VALUES
(1, 'T-Shirt', '1.jpg,2.jpg,3.jpg', 1500, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis animi, veritatis quae repudiandae quod nulla porro quidem, itaque quis quaerat!', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Quod, culpa!\r\nLorem ipsum dolor sit amet.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor qui eius esse!\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam, modi!\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Illum, iusto earum voluptates autem esse molestiae ipsam, atque quam amet similique ducimus aliquid voluptate perferendis, distinctio!\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis ea, voluptas! Aliquam facere quas cumque rerum dolore impedit, dicta ducimus repellat dignissimos, fugiat, minima quaerat necessitatibus? Optio adipisci ab, obcaecati, porro unde accusantium facilis repudiandae.', 's,m,l,xl,xxl', '#008000,#222222,#eeeeee', 1, NULL, NULL, 'female', 100, 1, '2019-03-16 14:20:07'),
(2, 'T-Shirt 2', '1.jpg,2.jpg,3.jpg', 1200, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis animi, veritatis quae repudiandae quod nulla porro quidem, itaque quis quaerat!', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Quod, culpa!\r\nLorem ipsum dolor sit amet.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor qui eius esse!\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam, modi!\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Illum, iusto earum voluptates autem esse molestiae ipsam, atque quam amet similique ducimus aliquid voluptate perferendis, distinctio!\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis ea, voluptas! Aliquam facere quas cumque rerum dolore impedit, dicta ducimus repellat dignissimos, fugiat, minima quaerat necessitatibus? Optio adipisci ab, obcaecati, porro unde accusantium facilis repudiandae.', 's,m,l,xl,xxl', '#008000,#222222,#eeeeee', 1, NULL, NULL, 'female', 0, 1, '2019-03-16 14:20:07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

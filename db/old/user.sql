-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 16, 2019 at 10:52 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `construction`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `job` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(32) NOT NULL,
  `role` varchar(15) NOT NULL,
  `pic` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `added` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `job`, `email`, `username`, `password`, `role`, `pic`, `status`, `added`) VALUES
(4, 'salinda jayawardana', 'Accountant', 'jayawardanasalinda@gmail.com', 'admin', '4297f44b13955235245b2497399d7a93', 'super_admin', 'sample.png', 1, 'a'),
(6, 'jdfgvsdgfj', 'knvjdsbj', 'kmjvnbjksd', 'dvjdb', 'vnkd', 'user', 'sample.png', 1, 'i'),
(7, 'salinda', 'Quality controler', 'salinda@3cs.aisa', 'xzxas', '698d51a19d8a121ce581499d7b701668', 'user', 'sample.png', 1, 'a'),
(8, 'sali', 'Stock keeper', 'salinda@3cs.aisa', 'sali', '81dc9bdb52d04dc20036dbd8313ed055', 'user', 'sample.png', 1, 'a');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
